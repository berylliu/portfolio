-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 2018-01-10 09:01:08
-- 服务器版本： 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qingl078_bery`
--

-- --------------------------------------------------------

--
-- 表的结构 `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_comment` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_name`, `contact_email`, `contact_comment`) VALUES
(1, 'test', 'test@fa.ca', ''),
(2, '345376', '4705508@qq.com', 'call me latter'),
(3, 'aa', '123@gmail.com', '  '),
(4, 'Lam', 'lam784@gmail.com', 'Hi');

-- --------------------------------------------------------

--
-- 表的结构 `portfolio_photos`
--

CREATE TABLE `portfolio_photos` (
  `id` int(11) UNSIGNED NOT NULL,
  `picture` varchar(255) NOT NULL,
  `lightbox_image` varchar(250) DEFAULT NULL,
  `image_desc` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `portfolio_photos`
--

INSERT INTO `portfolio_photos` (`id`, `picture`, `lightbox_image`, `image_desc`) VALUES
(1, './image/1-1.jpg', './image/lightbox/the-lord-of-the-rings.jpg', '<h5>The Lord of the Rings</h5>      \r\n<p>2017/04</p>\r\n<p>This responsive website was created for anniversary. This is my second semester final project.</p>\r\n<p>Media processing: Illustrator, Photoshop, After Effects.</p>\r\n<p>Web technology: HTML, CSS, JavaScript.</p>\r\n'),
(2, './image/1-2.jpg', './image/lightbox/lucky-garden.jpg', '<h5>Lucky Garden</h5>\r\n<p>2016/12</p>\r\n<p>This website was created for my friend\'s restaurant. This is my first semester final project.</p>\r\n<p>Media processing: Illustrator, Photoshop, After Effects.</p>\r\n<p>Web technology: HTML, CSS.</p>\r\n'),
(3, './image/1-3.jpg', './image/lightbox/flashback.jpg', '<h5>Flashback</h5>\r\n<p>2017/12</p>\r\n<p>This website was designed for part of Roku streaming service-Flashback.</p>\r\n<p>Media processing: Illustrator, Photoshop.</p>\r\n'),
(4, './image/2-1.jpg', './image/lightbox/tattoo.jpg', '<h5>Tattoo</h5>\r\n<p>2016/10</p>\r\n<p>Guangong and Feitian were drew by hand.</p>\r\n<p>Media processing: Illustrator, Photoshop.</p>\r\n'),
(15, './image/2-2.jpg', './image/lightbox/car.jpg', '<h5>Car</h5>\r\n<p>2017/12</p>\r\n<p>Media processing: Cinema 4D, After Effects.</p>\r\n'),
(6, './image/2-3.jpg', './image/lightbox/camera.jpg', '<h5>Photo</h5>\r\n<p>2017/02</p>\r\n<p>This photo took in spring.</p>\r\n<p>Media processing: Photoshop.</p>\r\n'),
(7, './image/3-1.jpg', './image/lightbox/li\'s-life.jpg', '<h5>Li\'s Life</h5>\r\n<p>2017/03</p>\r\n<p>These logos are created from my father\'s name. Following his daily life, hobbies and work.</p>\r\n<p>Media processing: Photoshop.</p>\r\n'),
(8, './image/3-2.jpg', './image/lightbox/makeup.jpg', '<h5>Makeups</h5>\r\n<p>2017/10</p>\r\n<p>Media processing: Cinema 4D, After Effects.</p>\r\n'),
(9, './image/3-3.jpg', './image/lightbox/shoes-shop.jpg', '<h5>Shoes shop</h5>\r\n<p>2017/02</p>\r\n<p>This website was designed for footprint in trip.</p>\r\n<p>Media processing: Illustrator, Photoshop.</p>\r\n<p>Web technology: HTML, CSS.</p>\r\n'),
(10, './image/4-1.jpg', './image/lightbox/ae.jpg', '<h5>AE</h5>\r\n<p>2017/02</p>\r\n<p>Media processing: After Effects.</p>\r\n'),
(11, './image/4-2.jpg', './image/lightbox/youtube.jpg', '<h5>YouTube</h5>\r\n<p>2015/04</p>\r\n<p>Media processing: After Effects.</p>\r\n'),
(12, './image/4-3.jpg', './image/lightbox/world-cup.jpg', '<h5>Soccer</h5>\r\n<p>2017/11</p>\r\n<p>Media processing: Cinema 4D, After Effects.</p>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `portfolio_photos`
--
ALTER TABLE `portfolio_photos`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用表AUTO_INCREMENT `portfolio_photos`
--
ALTER TABLE `portfolio_photos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
