$(function () {
    // Main js
    $(".PORTFOLIO").on("click", "img", function () {
        var id = $(this).data('id');
        // get lightbox image and description from database
        $.get( "database.php?image="+id, function( data ) {
            data = JSON.parse(data);
            $(".fliexInner img").attr("src", data.lightbox_image);
            $(".fliexInner div").html(data.image_desc);
            $(".fliexInner div").css("color", "#fff");
            $(".fliex").fadeIn();
        });
    })
    $(".fliex img").click(function (e) {
        e.stopPropagation();
    })
    $(".fliex, #close_btn").click(function () {
        $(".fliex").fadeOut();
        $(".fliexInner img").attr("src", '');
    })
    //load images from database
    $.get("database.php", function (data) {
        var data = JSON.parse(data);
        var html = ""
        for (var i = 0; i < data.length; i++) {
            if (data[i]["images_desc"] !== "header") {
                html += "<div class='image-container'><img src='" + data[i]["picture"] + "' alt='my works' class='pointer' data-id='"+ data[i]['id'] +"'>" + "</div>";
            }
        }
        $(html).appendTo(".myProducts");
    });
    //submit contact form
    $("#contactForm").submit(function (e) {

        $(".contactInput").each(function () {
            if ($(this).val() == "") {
                $(this).css("background", "pink");
                $(".errorMsg").fadeIn();
                e.preventDefault();
            }
        });

        $(".contactInput").on("change", function () {
            if ($.trim($(this).val()) !== "") {
                $(this).css("background", "");
            }
        });

        if ($.trim($("#name").val()) !== "" && $.trim($("#email").val()) !== "") {
            $.post("database.php", {
                name: $("#name").val(),
                email: $("#email").val(),
                comment: $("#message").val()
            }, function (data) {
                $(".sucessMsg").html(data);
                $(".sucessMsg").fadeIn().delay(2000).fadeOut();
            });
        }
        e.preventDefault();
    });
    //click icon and automatically scroll to top
    $(".backTop").click(function () {
        $("html,body").animate({
            scrollTop: 0
        });
    });
    //navigate web page
    $("header ul li a").each(function () {
        $(this).click(function () {
            var currNav = $(this).attr("id");
            $("html,body").animate({
                scrollTop: $("#" + currNav + "Section").offset().top
            });
        });
    });
    //down arrow
    $("#downArrow").click(function () {
        $("html,body").animate({
            scrollTop: $("#aboutSection").offset().top
        });
    });
    //scroll to work gallery
    $("#read_more").click(function () {
        $("html,body").animate({
            scrollTop: $("#workSection").offset().top
        });
    });
});